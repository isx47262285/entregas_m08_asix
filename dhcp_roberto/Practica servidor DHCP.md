########################################################################
# nombre: roberto altamirano martinez 
# hisx: isx47262285
# descripcion:  PRACTICA DE MONTAR UN SERVIDOR DHCP
########################################################################




L'empresa NúvolNET es trasllada i cal fer la instal·lació de xarxa en la que un servidor DHCP controlarà totes les adreces IP que hi ha a l'oficina
Caldrà configurar 60 punts de treball i a cada lloc de treball hi ha d'haver 
  --> 3 dispositius connectables: un Desktop, un portàtil i un mòbil
  --> Reserveu 2 IP per a les impressores de xarxa
  
  
Què hem de tenir present per saber si podem instal·lar un DHCP al GNU/linux que fem servir?
    (quins requisits de configuració pels "paquets de difusió" hem de tenir)
    (com hem de tenir configurat el firewall/iptables)

  --> A la configuració del servidor DHCP exclourem les primeres 10 adreces de host de la xarxa
  --> Reservarem 25 adreces per a seveis del CPD local
  
La primera adreça host de la xarxa estarà assignada a la porta d'enllaç

La resta les podrà assignar el servidor DHCP que cedirà la IP a cada host durant 7 dies
Establirem 3 tipus de grups pels 3 diferents pools (desktop, laptops i mòbils)

Ens cal un servidor de noms? Raona la resposta.
Quina és la configuració 'model' que haurem de posar a les màquines clients? 
Serà el mateix per a Fedora que per a Windows 7 o 10

Com deshabilitem els DHCP del Router del nostre ISP i fem que delegui en el nostre servidor?( NO CAL )


#################################################################################################################

## primer paso 

Para nuetro servidor nos hara falta instalar en nuestra maquina local el daemon dhcp 

 dnf -y install dhcpd
 
 
DHCP (sigla en inglés de Dynamic Host Configuration Protocol – Protocolo de configuración dinámica de host) 

## segundo paso 

ahora necesitamos encontrar un documento de configuracion ejemplo para realizar nuestra primera configuracion del servidor dhcp 

que se encontrara en: /usr/share/doc/dhcp-server/dhcpd.conf.example 

a partir de aqui podemos hacernos una idea de por donde iran los tiros de nuestra configuracion 

```
# This is a very basic subnet declaration.

subnet 10.254.239.0 netmask 255.255.255.224 {
  range 10.254.239.10 10.254.239.20;
  option routers rtr-239-0-1.example.org, rtr-239-0-2.example.org;
}

# This declaration allows BOOTP clients to get dynamic addresses,
# which we don't really recommend.

subnet 10.254.239.32 netmask 255.255.255.224 {
  range dynamic-bootp 10.254.239.40 10.254.239.60;
  option broadcast-address 10.254.239.31;
  option routers rtr-239-32-1.example.org;
}

# A slightly different configuration for an internal subnet.
subnet 10.5.5.0 netmask 255.255.255.224 {
  range 10.5.5.26 10.5.5.30;
  option domain-name-servers ns1.internal.example.org;
  option domain-name "internal.example.org";
  option routers 10.5.5.1;
  option broadcast-address 10.5.5.31;
  default-lease-time 600;
  max-lease-time 7200;
}
```
esta parte es solo un trozo del documento, en definitiva una de las partes basicas a tener en cuenta para nuestra configuracion 

## preparacion de la configuracion 

> primero debemos seleccionar nuestra tarjeta de red por la que vamos a trabajar 
en mi caso personal, trabajare contra mi tarjeta de red de cable (RJ45) de mi laptop 

```
2: enp6s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:24:54:e0:a2:fa brd ff:ff:ff:ff:ff:ff
```
el porque la tarjeta de red de cableado, es basicamente para estar seguros de que es la puerta de enlace correcta
y que por otra parte cuando se conecta el daemon dhclient actua sobre el y le la ip ( lo cual es el objetivo a conseguir )


> tomada la decision vamos a elegir una red en la que trabajar 

	10.0.0.0/24  --> red seleccionada 
	10.0.0.1/24  --> ip de la puerta de enlace 
	
```	
[root@localhost dhcp-server]# ip a add 10.0.0.1/24 dev enp6s0

2: enp6s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:24:54:e0:a2:fa brd ff:ff:ff:ff:ff:ff
    inet 10.0.0.1/24 scope global enp6s0
       valid_lft forever preferred_lft forever
```

## tercer paso 

hacer una configuracion basica y comprovar que nuestro servidor dhcp nos da ip a nuestro host cliente 

para esto hemos configurado de la siguiente manera el dhcp.conf

```
[root@localhost dhcp]# cat dhcpd.conf.ecenari1 
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#

default-lease-time 200;
max-lease-time 800;

class "equip-aulaI" {
max-lease-time 2000;}

subnet 10.0.0.0 netmask 255.255.255.0 { 
	pool { range 10.0.0.10 10.0.0.50;
	option routers 10.0.0.1;
	option subnet-mask 255.255.255.0;
	option domain-name "dchp charli";
	option domain-name-servers 8.8.8.8;
	 }
}

```

# ultimo paso es arrancar nuestro servidor dhpcd
```
[root@localhost dhcp]# systemctl start dhcpd
```
> importante: si existe una  modificacion o un error cabe hacer un restart del servicio 
```
[root@localhost dhcp]# systemctl restart dhcpd
```

finalmente vemos que si tenemos ip  en el host 
apartir de este paso comprovamos que tenemos un servidor que hace lo que necesitamos 
nos ponemos en marcha para configurar las peticiones de nuestra empresa nuvolnet.

ejemplo:

```
2: enp6s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:24:54:e0:a2:fa brd ff:ff:ff:ff:ff:ff
    inet 10.0.0.100/24 scope global enp6s0
       valid_lft forever preferred_lft forever
```

```
[root@localhost ~]# cat /etc/resolv.conf
# Generated by NetworkManager
search dhcp.xarli
nameserver 8.8.8.8
```


## Ens cal un servidor de noms? Raona la resposta. 

si, porque parte importante de un servidor dhcp es un servidor dns 
el cual puede ser uno local propio de nuestra empresa con configuracion  privada de los host de la empresa 
y/o un servidor dns universal como es el caso de 8.8.8.8 

en el momento que tenemos conexion hacia el exterior haciendo nat y masquerade hacia la red 
necesitamos un servidor de nombres si o si para la navegacion ! 


## Quina és la configuració 'model' que haurem de posar a les màquines clients? 

* en este apartado tendriamos en cuenta que la configuracion modelo, seria una basada en los host-name de todos los host conectados
* en el cual podemos configurar unos strings de host-name que hagan match para poder conectarnos con total seguridad al pool correcto 

el ejemplo:  

el cliente debe tener la siguiente configuracion:

```
Editar fitxer dhclient.conf (si no exiteix crear-lo a /etc/dhcp/) i afegir:
		
		send host-name "dep02";
		send max-lease-time 2000;

```

de esta manera aseguramos los hosts y tambien la seleccion correcta de los pools

* por defecto envian una solicitud de dhcp cliente en la cual  la cabecera del paquete cliente envia datos como las macs y tambien los host-name
* en definitiva la clave de la configuracion pasa por configurar los host-name de los clientes a conectar en nuestro servido dhcp


[root@localhost dhcp]#  cat /etc/hostname
localhost.localdomain


## on deshabilitem els DHCP del Router del nostre ISP i fem que delegui en el nostre servidor?( NO CAL )

en mi caso personal no he podido acceder como administrador al router isp de mi compañia,

pero esto no me ha supuesto ningun problema para salir a internet y tampoco para dar ips privadas a mi red local

por mi parte como he dicho he trabajado con mi tarjeta de red local  y hemos conseguido el objetivo 
dar ips , traspasar la informacion de ruta a la puerta de la enlace, servidor dns   y ademas obtener salia al exterior  simplemente modificando nuestra tabla de ip tables 

# un ejemplo de nuestra salida a internet despues de modificar iptables


```
[root@dep01 ~]# ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=121 time=17.8 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=121 time=16.8 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=121 time=29.4 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=121 time=21.0 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 16.835/21.278/29.413/4.946 ms
```

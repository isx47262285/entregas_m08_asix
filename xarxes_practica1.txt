ENTREGA

    Configureu dos ordinadors fent servir la comanda ip amb dues adreces del rang 172.16.0.0 i comproveu que es veuen entre si. Indiqueu les comandes que heu utilitzat a)per configurar-les b) per comprovar que es veuen.
    Com podem descobrir l'adreça mac d'un altre dispositiu de la nostra xarxa (que no sigui el nostre)? Indiqueu-ne les comandes i la sortida.
    Com podem canviar el nom d'una interfície de xarxa amb la comanda ip? Poseu exemple
    Busqueu el paquet RPM per a la utilitat netperf i instal.leu-lo (copieu netperf al directori cp netperf /usr/local/bin per tal que sigui accessible desde qualsevol lloc).
     --Arrenqueu el servidor en una de les màquines i el client en l'altra tot comprovant la velocitat que obtenim entre els dos. 
     --Indiqueu quina es la velocitat obtinguda. Feu servir aquest script en el client per tal de comprovar TCP full-duplex:
         for i in 1
             do
              netperf -H 10.1.2.33 -t TCP_STREAM -B "outbound" -i 10 -P 0 -v 0 \
                -- -s 256K -S 256K &
              netperf -H 10.1.2.33 -t TCP_MAERTS -B "inbound"  -i 10 -P 0 -v 0 \
                -- -s 256K -S 256K &
             done
    Canvieu ara la MTU de les interfícies de xarxa dels dos ordinadors a un valor de 9000 bytes. comproveu de nou quina és la velocitat obtinguda. Expliqueu els resultats tot comparant-los.
    Realitzeu ara la mateixa configuració estàtica d'adreces IP amb la comanda nmcli. Indiqueu-ne els passos.
    On es troba la configuració de servidors de noms de domini?
    Realitzeu la mateixa configuració anterior de manera permanent (ifcfg-...). Quins són els paràmetres de la comanda nmcli per tal de forçar que s'apliquin els canvis de xarxa que hem fet als fitxers?
    Com podem canviar de manera permanent el nom de les interfícies de xarxa (que sobrevisquin a un reinici?) /etc/udev/rules.d/70-persistent-net.rules
        SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="38:1c:4a:c5:9a:a7", ATTR{type}=="1", KERNEL=="e*", NAME="mi-interfaz"

    Com podem traduïr un nom de domini per la seva/seves adreces IP? I com podem trobar el nom de domini a partir d'una adreça IP?


Indiqueu les comandes que heu utilitzat 
a)per configurar-les

primer esborrem les ips que hi ha configurades

#ip a flush enp5s0

segon configurem la nostra ip al host 

#ip a add 172.16.0.15/24 dev enp5s0


 
b) per comprovar que es veuen.


ping 172.16.0.14

[root@i15 ~]# ping 172.16.0.14
PING 172.16.0.14 (172.16.0.14) 56(84) bytes of data.
64 bytes from 172.16.0.14: icmp_seq=1 ttl=64 time=0.433 ms
64 bytes from 172.16.0.14: icmp_seq=2 ttl=64 time=0.483 ms
^C


com que som de la mateixa xarxa 172.16.0.0/24 ens veiem!!

-------------------------------------------------------------------------

Com podem descobrir l'adreça mac d'un altre dispositiu de la nostra xarxa (que no sigui el nostre)? 

# ip neigh 
172.16.0.14 dev enp5s0 lladdr 40:8d:5c:e4:30:4e STALE

 Com podem canviar el nom d'una interfície de xarxa amb la comanda ip? Poseu exemple??


[root@i15 ~]# ip link set enp5s0 down 
[root@i15 ~]# ip link set enp5s0 name clase15

[root@i15 ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: clase15: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 40:8d:5c:e4:37:c7 brd ff:ff:ff:ff:ff:ff



Busqueu el paquet RPM per a la utilitat netperf i instal.leu-lo (copieu netperf al directori cp netperf /usr/local/bin per tal que sigui accessible desde qualsevol lloc).



--Arrenqueu el servidor en una de les màquines i el client en l'altra tot comprovant la velocitat que obtenim entre els dos. 


# /usr/bin/netserver


-- comprovem que esta escoltan el nostre servidor



[root@i15 ~]# netstat  -tlnp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1872/cupsd          
tcp6       0      0 ::1:631                 :::*                    LISTEN      1872/cupsd          
tcp6       0      0 :::12865                :::*                    LISTEN      8299/netserver    


--Indiqueu quina es la velocitat obtinguda. Feu servir aquest script en el client per tal de comprovar TCP full-duplex:



#! /bin/bash

for i in 1
do
  netperf -H 172.16.0.15 -t TCP_STREAM -B "outbound" -i 10 -P 0 -v 0 -- -s 256K -S 256K &
  netperf -H 172.16.0.14 -t TCP_MAERTS -B "inbound"  -i 10 -P 0 -v 0 -- -s 256K -S 256K &

done

**executem el script


#bash script_netperf.sh 


[isx47262285@i15 xarxes]$ !!! WARNING
!!! Desired confidence was not achieved within the specified iterations.
!!! This implies that there was variability in the test environment that
!!! must be investigated before going further.
!!! Confidence intervals: Throughput      : 30.335%
!!!                       Local CPU util  : 0.000%
!!!                       Remote CPU util : 0.000%

59426.40 outbound
!!! WARNING
!!! Desired confidence was not achieved within the specified iterations.
!!! This implies that there was variability in the test environment that
!!! must be investigated before going further.
!!! Confidence intervals: Throughput      : 23.779%
!!!                       Local CPU util  : 0.000%
!!!                       Remote CPU util : 0.000%

58585.73 outbound
!!! WARNING
!!! Desired confidence was not achieved within the specified iterations.
!!! This implies that there was variability in the test environment that
!!! must be investigated before going further.
!!! Confidence intervals: Throughput      : 30.528%
!!!                       Local CPU util  : 0.000%
!!!                       Remote CPU util : 0.000%

59870.32 outbound
!!! WARNING
!!! Desired confidence was not achieved within the specified iterations.
!!! This implies that there was variability in the test environment that
!!! must be investigated before going further.
!!! Confidence intervals: Throughput      : 36.748%
!!!                       Local CPU util  : 0.000%
!!!                       Remote CPU util : 0.000%

  86.94 inbound


** fem comprovacions a pelo 


[root@i15 ~]# netstat -tlnp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1872/cupsd          
tcp6       0      0 ::1:631                 :::*                    LISTEN      1872/cupsd          
tcp6       0      0 :::12865                :::*                    LISTEN      8299/netserver      
[root@i15 ~]#  netperf -H 172.16.0.15 -t TCP_STREAM -B "outbound" -i 10 -P 0 -v 0 -- -s 256K -S 256K &
[1] 8666
[root@i15 ~]# netstat -tlnp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:35723           0.0.0.0:*               LISTEN      8667/netserver      
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1872/cupsd          
tcp6       0      0 ::1:631                 :::*                    LISTEN      1872/cupsd          
tcp6       0      0 :::12865                :::*                    LISTEN      8299/netserver      
[root@i15 ~]# 72089.78 outbound




---------------------------------------------------------------------------------------------------------------------------------------------------
 Canvieu ara la MTU de les interfícies de xarxa dels dos ordinadors a un valor de 9000 bytes. comproveu de nou quina és la velocitat obtinguda. Expliqueu els resultats tot comparant-los.


#ip link set enp5s0 mtu 9000


[root@i15 ~]#  netperf -H 172.16.0.15 -t TCP_STREAM -B "outbound" -i 10 -P 0 -v 0 -- -s 32K -S 32K &
[1] 8696
[root@i15 ~]# 36139.92 outbound

Confidence intervals: Throughput      : 125.548%


** cambia la velocitat de resposta y tambe millora el percentatge de confidence


Realitzeu ara la mateixa configuració estàtica d'adreces IP amb la comanda nmcli. Indiqueu-ne els passos.


# nmcli connection modify enp5s0 ipv4.addresses 172.16.0.15/24 gw4 172.16.0.1


** una vez hecho le cambio no ha sido efectivo hay que tirar la interficia abajo y subirla otra vez

[root@i15 ~]# ip link set enp5s0 down 
[root@i15 ~]# ip link set enp5s0 up

** comprovamos 

root@i15 ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp5s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9000 qdisc fq_codel state UP group default qlen 1000
    link/ether 40:8d:5c:e4:30:4e brd ff:ff:ff:ff:ff:ff
    inet 192.168.2.45/16 brd 192.168.255.255 scope global dynamic enp5s0
       valid_lft 21599sec preferred_lft 21599sec
    inet 172.16.0.15/24 brd 172.16.0.255 scope global enp5s0
       valid_lft forever preferred_lft forever
    inet6 fe80::428d:5cff:fee4:304e/64 scope link tentative 
       valid_lft forever preferred_lft forever



    On es troba la configuració de servidors de noms de domini?

[isx47262285@i15 ldapserver]$ cat /etc/resolv.conf 
# Generated by NetworkManager
search informatica.escoladeltreball.org
nameserver 192.168.0.10
nameserver 10.1.1.200
nameserver 208.67.222.222
[isx47262285@i15 ldapserver]$ 




    Realitzeu la mateixa configuració anterior de manera permanent (ifcfg-...). Quins són els paràmetres de la comanda nmcli per tal de forçar que s'apliquin els canvis de xarxa que hem fet als fitxers?



** ficheros de configuracion de red estan en  /etc/sysconfig/network-scripts
** comprovem el fitxer de configuracio de la interficie enp5s0

[isx47262285@i15 ldapserver]$ cat /etc/sysconfig/network-scripts/ifcfg-enp5s0 
# Generated by dracut initrd
NAME="enp5s0"
DEVICE="enp5s0"
ONBOOT=yes
NETBOOT=yes
UUID="dd711266-bb71-4d49-9533-ac3ec6829f83"
IPV6INIT=yes
BOOTPROTO=dhcp
TYPE=Ethernet




** para que la configuracion permanente sea la nuestra y no la configurada por defecto haremos los siguiente pasos



# nmcli con mod enp5s0 connection.autoconnect no


** creamos un nuevo perfil 


# nmcli con add type ethernet con-name new_profile ifname enp5s0 ip4 172.16.0.14/24 gw4 172.16.0.1



[root@i15 ~]# cat /etc/sysconfig/network-scripts/ifcfg-new_profile 
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=none
IPADDR=172.16.0.15
PREFIX=24
GATEWAY=172.16.0.1
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=new_profile
UUID=dd711266-bb71-4d49-9533-ac3ec6829f83
DEVICE=enp5s0
ONBOOT=yes

** automaticamente hemos registrado un nuevo perfil de configuracion directamente para le interficie enp5s0



 Com podem canviar de manera permanent el nom de les interfícies de xarxa (que sobrevisquin a un reinici?) /etc/udev/rules.d/70-persistent-net.rules
        SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="38:1c:4a:c5:9a:a7", ATTR{type}=="1", KERNEL=="e*", NAME="mi-interfaz"



SUBSYSTEM=="net", ACTION=="add",ATTR{address}=="02:42:96:0d:84:33",NAME="mi_red"


    Com podem traduïr un nom de domini per la seva/seves adreces IP? I com podem trobar el nom de domini a partir d'una adreça IP?

** amb la comanda host saben quina es la ip 


[isx47262285@i15 ldapserver]$ host google.com
google.com has address 172.217.16.238
google.com has IPv6 address 2a00:1450:4003:809::200e
google.com mail is handled by 30 alt2.aspmx.l.google.com.
google.com mail is handled by 50 alt4.aspmx.l.google.com.
google.com mail is handled by 20 alt1.aspmx.l.google.com.
google.com mail is handled by 40 alt3.aspmx.l.google.com.
google.com mail is handled by 10 aspmx.l.google.com.
[isx47262285@i15 ldapserver]$ 








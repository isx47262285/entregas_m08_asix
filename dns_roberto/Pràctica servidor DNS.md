Hem d'instal.lar i configurar un servidor de noms de domini (DNS) (3 opcions). Cal aconseguir els següents objectius:
    • Justifica perquè has triat fer-lo a l'ordinador físic, a un contenidor docker o a una màquina virtual
    • El servei de DNS s'ha d'arrencar en iniciar l'ordinador.
    • Serà autoritari per a la zona que vosaltres creeu i haurà de tenir, com a mínim, la resolució de noms de les màquines:
        • www
        • dhcp
        • dns
        • mail (servidor de correu)
    • Aquestes màquines han de resoldre per la ip de la vostra màquina.
    • També cal que es resolgui l'adreça de la hipotètica màquina <examen> per la ip 192.168.8.8
    • Haurà de ser autoritatiu de la vostra zona i cal configurar el fitxer de zona.
    • Cal que tingui el fitxer de zona inversa in.addr.arp amb tots els hosts configurats al fitxer de zona.
    
    AQUESTA PRÀCTICA TINDRÀ MÉS APARTATS
    CAL FER LA ENTREGA EN UN DOCUMENT QUE TINGUI CAPTURES DE PANTALLA QUE DEMOSTRIN QUE HEU FET LA FEINA DE MANERA INDIVIDUAL
    
    DATA D'ENTREGA 23/11/2018
    


1 PAS, SELECCIONAMOS COMO METODO OPERACIONAL UN DOCKER EN FEDORA 27

* para realizar esta configuracion de servidor dns vamos a utilizar nuestra maquina real dado que la opcion de docker ha sido un fracaso por la conectividad del container 


> primero la intalacion del daemon que controla el servidor 
-- el daemon  que controla el servidor dns sera named
```
[root@localhost /]# dnf -y install bind bind-utils

[root@localhost named]# locate named 
/etc/named
/etc/named.conf  --> fitxer de configuracio *******
/etc/named.rfc1912.zones
/etc/named.root.key
/etc/logrotate.d/named
/etc/rwtab.d/named
/etc/sysconfig/named
/run/named
/usr/bin/named-rrchecker
/usr/lib/systemd/systemd-hostnamed
/usr/lib/systemd/system/named-setup-rndc.service
/usr/lib/systemd/system/named.service
/usr/lib/systemd/system/systemd-hostnamed.service
/usr/lib/tmpfiles.d/named.conf
/usr/sbin/named   --> servei
/usr/sbin/named-checkconf
/usr/sbin/named-journalprint
/usr/share/doc/bind/sample/var/named
/usr/share/doc/bind/sample/var/named/data
/usr/share/vim/vim81/syntax/named.vim
/var/named
/var/named/data
/var/named/dynamic
/var/named/named.ca
/var/named/named.empty
/var/named/named.localhost
/var/named/named.loopback
/var/named/slaves


```



-- una vegada trobat tot,  estudiamos el servidor dns que esta configurado en el siguiente fichero 

```

[root@localhost ~]# cat /etc/named.conf   --> configuracio basica

//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//

options {
	listen-on port 53 { 127.0.0.1; };
	listen-on-v6 port 53 { ::1; };
	directory 	"/var/named";
	dump-file 	"/var/named/data/cache_dump.db";
	statistics-file "/var/named/data/named_stats.txt";
	memstatistics-file "/var/named/data/named_mem_stats.txt";
	allow-query     { localhost; };

	/* 
	 - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
	 - If you are building a RECURSIVE (caching) DNS server, you need to enable 
	   recursion. 
	 - If your recursive DNS server has a public IP address, you MUST enable access 
	   control to limit queries to your legitimate users. Failing to do so will
	   cause your server to become part of large scale DNS amplification 
	   attacks. Implementing BCP38 within your network would greatly
	   reduce such attack surface 
	*/
	recursion yes;

	dnssec-enable yes;
	dnssec-validation yes;

	managed-keys-directory "/var/named/dynamic";

	pid-file "/run/named/named.pid";
	session-keyfile "/run/named/session.key";

	/* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
	include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
	type hint;
	file "named.ca";
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

> apartir de este punto comenzamos nuestra configuracion directa de dns 

2. • El servei de DNS s'ha d'arrencar en iniciar l'ordinador.

* para que nuestro daemon se arranque directamente al arrancar el sistema haremos la siguiente orden

```
# systemctl enable named
```

con esta opcion se arrancara el daemon cuando volvamos a iniciar nuestro sistema pero ahora mismo el daemon esta parado 
para arrancarlo directamente haremos la siguiente orden :


```
# systemctl start named
```

en estos momentos el daemon esta arrancado pero no tiene ninguna configuracion con lo cual no hara ningun servicio de forma directa


3.• Serà autoritari per a la zona que vosaltres creeu i haurà de tenir, com a mínim, la resolució de noms de les màquines:
        • www
        • dhcp
        • dns
        • mail (servidor de correu)

apartir de aqui tenemos que configurar nuestro dns, para esto vamos a modificar nuestro fichero /etc/named.conf
con la siguiente configuracion 

```

// CONFIGURACION DNS ROBERTO ALTAMIRANO
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//

options {
        listen-on port 53 { 127.0.0.1; 192.168.2.45; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { any; };
        allow-query-cache {any; };
    


        /* 
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable 
           recursion. 
         - If your recursive DNS server has a public IP address, you MUST enable access 
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification 
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface 
        */
        recursion no;



        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "roberto.miempresa.org" IN {
        notify no;
        type master;
        file "roberto.miempresa.org.zone";
       
};


zone "1.0.10.in-addr.arpa" IN {
        notify no;
        type master;
        file "roberto.miempresa.org.rev.zone";
       
};

zone "8.168.192.in-addr.arpa" IN {
        notify no;
        type master;
        file "roberto.miempresa2.org.rev.zone";
};



include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";

                                                                                                                                                                                                                                                                          1,1           Top
```


lo primero colocar nuestra ip como servidor dns 192.168.2.45 (ip de ordenador de clase )

apartir de aqui hay varias directrices importantes que activar:
```
 allow-query     { any; };
recursion no;

```

estas dos son muy importantes, la primera nos permitira hacer consultas desde fuera, es decir, cualquier host podra hacer consultas dns 
la segunda opcion recursion no, para que directamente la contestacion solo sea autoritario de nuestro dns.


y finalmente la parte mas importante que es la declaracion de zonas de resolucion, en la cual habra una resolucion directa y una resolucion inversa 
esto se traduce  en:   1.- que podremos resolver consultas mediante una ip : 10.0.1.1  
						2.- que podremos resolver dominios directamente  www.roberto.miempresa.org 
						
```
zone "roberto.miempresa.org" IN {
        notify no;
        type master;
        file "roberto.miempresa.org.zone";
       
};


zone "1.0.10.in-addr.arpa" IN {
        notify no;
        type master;
        file "roberto.miempresa.org.rev.zone";
       
};

zone "8.168.192.in-addr.arpa" IN {
        notify no;
        type master;
        file "roberto.miempresa2.org.rev.zone";
};


```

lo importante sera  definir que tipo de servidor es....  :  type master
y lo segundo el nombre de los ficheros que contendran nuestra configuracion de zonas directas e indirectas 

estas zonas se encontraran en el directorio /var/named 



para comprovar que las configuraciones son correctas  el daemon bind nos facilita unas herramientas muy utiles:

named-checkconf
named-checkzone


### configuracion de zonas 

## zona directa 
```
; Fichero de configuración de los host para la zona:
; 	roberto.miempresa.org  (10.0.1.0/24)

$TTL 3D
@ IN SOA ns.roberto.miempresa.org. admin.roberto.miempresa.org. 1 3H 15M 1W 1D
	NS 	ns
	MX	10 mailhost
	; A 10.0.1.1

; sevicios de red
ns		A 	10.0.1.1
mailhost	A	10.0.1.1
ex	A	192.168.8.8

router	CNAME	ns
server	CNAME	ns
www	CNAME	ns
dns	CNAME	ns
dhcp	CNAME	ns

; hosts clientes de la red
pc1	A 10.0.1.3
pc2	A 10.0.1.4
pc3     A 10.0.1.5
pc4 	A 10.0.1.6
server-examen	CNAME	ex
```
## zona inversa 
```
; zona 8.168.192.in-aadr.arpa.
; Configuracion inversa de la maquina <examen>

$TTL 3D
8.168.192.in-addr.arpa.  IN SOA ns.roberto.miempresa.org. admin.roberto.miempresa.org. 1 3M 1M 1W 1D
    NS ns.roberto.miempresa.org.

; llistat dels servidors i components troncals
8	PTR server-examen

```

una vez sabemos que todo ha ido bien podemoas hacer un restart del sistema y hacer consultas dns 

```
# systemctl restart named 
```


# comprovacion de dns 
```
[root@localhost named]# dig @192.168.2.45 -x 192.168.8.8 +short
server-examen.8.168.192.in-addr.arpa.

```

* Resolviendo ip pc1

```
[root@localhost named]# dig @192.168.2.45 -x 172.17.1.3 +short
pc1.1.17.172.in-addr.arpa.

```
* Le preguntamos directamente por el pc1.jose.miempresa.org

```
[root@192 ~]# nslookup ns.roberto.miempresa.org 192.168.2.45
Server:		192.168.2.45
Address:	192.168.2.45#53

Name:	ns.roberto.miempresa.org
Address: 10.0.1.1

```



###  caso de consultas cache 

las consultas cache tienen una funcion que no  es mas que se guardan las consultas dns previas en el cache 
con lo cual cuando hagamos otra resolucion hacia el mismo host tendremos la informacion con mas rapidez
solo necesitamos tener activada la siguiente directiva 


allow-query-cache {any; };

#### caso de consultas y forwaders 

las consultas con forwaders significa que en primera instancia resolveremos nuestas zonas y si no encontramos la consulta en nuestras zonas 
vamos a redirigir la consulta a un servidor dns "amigo"  un ejemplo google (8.8.8.8)
para esto solo tenemos que activar la directiva de forwader con la ip del servidor amigo  en nuestro named.conf

	   recursion yes ;

       forwarders { 8.8.8.8; 8.8.4.4; };
       

        
###  caso 3 y mas complejo 

## master slave 

esta configuracion tiene una utilidad muy eficiente y es que podamos seguir resolviendo consultas dns aunque nuestro servidor se nos haya caido

manera de hacerlo ???

necesitamos dos servidores dns 1 master y 2 slave 

la confguracion del master seria identica a la primera pero añadiendo unas directivas muy importantes:

```
// CONFIGURACION DNS ROBERTO ALTAMIRANO
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//

options {
        listen-on port 53 { 127.0.0.1; 192.168.2.45; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { any; };
        allow-query-cache {any; };
        forwarders { 8.8.8.8; 8.8.4.4; };
        allow-transfer { 192.168.2.44; }; // ip del slave


        /* 
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable 
           recursion. 
         - If your recursive DNS server has a public IP address, you MUST enable access 
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification 
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface 
        */
        recursion yes;



        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "roberto.miempresa.org" IN {
        notify no;
        type master;
        file "roberto.miempresa.org.zone";
        allow-transfer { 192.168.2.44; };
};


zone "1.0.10.in-addr.arpa" IN {
        notify no;
        type master;
        file "roberto.miempresa.org.rev.zone";
        allow-transfer { 192.168.2.44; };
};

zone "8.168.192.in-addr.arpa" IN {
        notify no;
        type master;
        file "roberto.miempresa2.org.rev.zone";
        allow-transfer { 192.168.2.44; };
};



include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```


importante definir bien las directivas 

  allow-transfer { 192.168.2.44; }; // ip del slave



zone "roberto.miempresa.org" IN {
        notify no;
        type master;
        file "roberto.miempresa.org.zone";
        allow-transfer { 192.168.2.44; };
};


tanto en las options como en las zonas han de estar declarada la ip de nuestro servidor secundario (slave )

y en la parte del slave otra configuracion named.conf :

```

// CONFIGURACIÓ SLAVE
// named.conf
// server domini: x1.edt.org
//

options {
	listen-on port 53 { 127.0.0.1; 192.168.2.44; };
	listen-on-v6 port 53 { ::1; };
	directory 	"/var/named";
	dump-file 	"/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";

        // acl corpnets { 192.168.4.0/24; 192.168.5.0/24; };

	// a) exemple de configuració de servidor dns aillat, només fa respostes
        // autoritatives de la seva zona pròpia. No pot respondre a consultes 
        // d'altres dominis
        // no cal caché perquè no busca fora 
        // no cal recursió perquè no busca fora
        // no es defineix cap forwarder a qui passar-li la pilot       
        //   allow-query  { any; };
        //   recursion no;
        // ////////////////////////////////////////////////////////////////////

	// b) exemple de configuració de servidor només caché. 
        // indicar quines consultes es poden desar al caché. Es permet la
        // recursió per resoldre les consultes consultant a l'exterior 
        // seguint el procés usual de recursio
          //allow-query-cache{ any;};  
          //allow-query { any; };
          //recursion yes;
            // ** nota ** cal desactiva les zones internes o locals: 
            // x1.edt.org  i 0.17.172.in-addr.arpa
        // ////////////////////////////////////////////////////////////////////

        // c) exemple de configuració de servidor zona + caché-recursió. 
        // indicar quines consultes es poden desar al caché. Es permet la
        // recursió per resoldre les consultes a altres zones. Es fa la 
        // resolució de les zones locals
        //  allow-query-cache{ any;};  
        //  allow-query { any; };
        //  recursion yes;
        // ////////////////////////////////////////////////////////////////////
        
        // d) exemple de configuració de servidor només forward + si/no-cache
        // no té zona pròpia + si fa recursió (cal). Tot el que cal resoldre
        // es demana al/s forwarders.
        // si caché està activat recordarà les respostes que rebi, si no està
        // activat cada resolució (externa) es farà contactant al forwarder
          //allow-query-cache{ any;};   
          //allow-query { any; };
          //recursion yes;
          //forward only;
          //forwarders { 192.168.122.1; };  :
          // ** nota ** si no ha de resoldre la zona local cal comentar-la
        // ////////////////////////////////////////////////////////////////////

        // e) exemple de configuració de servidor zona local + forward + cache
        // es fa recursió per poder demanar al forwarder, però no recursió a 
        // fora. Tot el que cal resoldre és demana al/s forwarders.
        // si caché està activat recordarà les respostes que rebi, si no està
        // activat cada resolució (externa) es farà contactant al forwarder
          allow-query-cache{ any;};   
          allow-query { any; };
          recursion yes;

	// opcions servidor secundari
	  dnssec-enable yes;
	  dnssec-validation yes;

          //forwarders { 192.168.122.1; }; 
        // ////////////////////////////////////////////////////////////////////



	/* Path to ISC DLV key */
	bindkeys-file "/etc/named.iscdlv.key";
};

zone "." IN {
  type hint;
  file "named.ca";
};

include "/etc/named.rfc1912.zones";

zone "roberto.miempresa.org" {
  notify no;
  type slave;
  file "pollo1.db";
  masters { 192.168.2.45; };
};

zone "1.17.172.in-addr.arpa" {
  notify no;
  type slave;
  file "pollo2.db";
  masters { 192.168.2.45; };
};

zone "8.168.192.in-addr.arpa"{
  notify no;
  type slave;
  file "pollo3.db";
  masters { 192.168.2.45; };
};

```

esta configuracion la hemos realizado en conjunto con el host 192.168.2.44  para poder realizar las pruebas pertinentes

el resultado final es que aun el dns master estando apagado, el slave puede resolver todas las ips de las zonas declaradas 
ya que ha recopilado toda la informacion automaticamente desde su master 192.168.2.45 

conclusion, la resolucion dns es mucho mas util y mas rapida que la declaracion de host en el /etc/hosts

cabe tomar nota de que para comprovar estas consultas hemos comentado todos las ips de resolucion del /etc/resolv y asi asegurar el resultado.



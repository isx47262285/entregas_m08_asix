### Instalación de wordpress
#####  roberto  altamirano Martinez

```
cd /tmp
wget http://wordpress.org/latest.tar.gz
tar -xvzf latest.tar.gz -C /var/www/html
```
### Cambiar los permisos y el propietario de la carpeta wordpress

```
chown -R apache:apache /var/www/html/wordpress
chmod -R 777 /var/www/html/wordpress

```
### Instalar Mariadb

```
sudo dnf -y install Mariadb
systemctl start Mariadb.service
añadir usuario wordpress pass jupiter como usuario en mariadb usando phpmyAdmin
```

### Configuració

```
systemctl restart mariadb

mysql_secure_installation

systemctl restart http

```
### crear una base de dades de wordpress donde estatara nuestra web dinamica

> dentro de mariadb creamos la base de datos wordpress


### Comenzar instalación de wordpress


>desde el Navegador firefox "http://localhost/wordpress"  crear las credenciales y conexiones con la base de datos

si todo va bien 

usuario : root
passwd : jupiter


### Instalar plugins de generación de contenido

```
echo "define('FS_METHOD', 'direct');" >> wp-config.php
Instalar en la pestaña plugins del dashboard un plugin de generacion de contenido random, y utilizarlo.
```

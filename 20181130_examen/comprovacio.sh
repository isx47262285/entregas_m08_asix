#! /bin/bash
# nombre: roberto altamirano martinez
# hisx: isx47262285
# descripcion : programa en bash que recoge la info de comprovacion del servidor dns
# syntaxy: comprovacio.sh @server 
############################################################

server="$1"


if [ $# -ne 2 ] 
then
  echo "usage: comprovacio.sh @server"
  echo "server: @192.168.2.45"
fi 


# server 

echo "server : $server"

# comprovacio del /etc/resolv
echo -e  "\t /etc/resolv.conf  sin tocar  \n\n"  >> log.txt

cat /etc/resolv.conf >> log.txt

echo -e "\t /etc/host  \n\n" >> log.txt

cat /etc/hosts >> log.txt


echo -e "\n\n"
echo " ----------------------------------------------------------"  >> log.txt
echo " \t  comprovacion de host \n\n "  >> log.txt


dig $server -x 192.168.2.45 +short  >> log.txt

dig $server -x 10.123.0.10 +short  >> log.txt
dig $server -x 10.123.0.11 +short  >> log.txt
dig $server -x 10.123.0.21 +short  >> log.txt
dig $server -x 10.123.0.22 +short  >> log.txt
dig $server -x 10.123.0.31 +short  >> log.txt
dig $server -x 10.123.0.32 +short  >> log.txt


echo -e "\n \n"  >> log.txt
echo " ----------------------------------------------------" >> log.txt

echo -e  " \t reverse de las zonas" >> log.txt
  
nslookup impresora.mifp.cc 192.168.2.45  >> log.txt
nslookup ftp.mifp.cc 192.168.2.45  >> log.txt
nslookup a01.mifp.cc 192.168.2.45  >> log.txt
nslookup a02.mifp.cc 192.168.2.45  >> log.txt
nslookup p01.mifp.cc 192.168.2.45  >> log.txt
nslookup p02.mifp.cc 192.168.2.45  >> log.txt







